#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

int nextSmallerThan(int);
void nextSmallerThan(string, string);
void nextSmallerThanTests();
bool nextSmallerThanTests(string, string);
int countDigits(int number);

int main()
{
	nextSmallerThanTests();
	nextSmallerThan("input.txt", "result.txt");
	nextSmallerThanTests("input.txt", "result.txt");
	system("pause");
}

int nextSmallerThan(int number)
{
	string numberStr = to_string(number);
	int digits[10] = { 0 };

	for (int i = 0; i < numberStr.size(); i++) {
		digits[numberStr[i] - '0']++;
	}

	for (int i = 9; i >= 0; i--) {
		if (digits[i] > 0) {
			digits[i]--;

			int newNumber = 0;
			bool firstNonZero = false;
			for (int j = 0; j < 10; j++) {
				if (digits[j] > 0) {
					if (!firstNonZero && j > i) {
						continue;
					}
					firstNonZero = true;
					for (int k = 0; k < digits[j]; k++) {
						newNumber = newNumber * 10 + j;
					}
				}
			}

			if (newNumber < number) {
				return newNumber;
			}

			digits[i]++;
		}
	}

	return -1;
}

void nextSmallerThan(string input, string output)
{
	ofstream outputFile(output);

	ifstream inputFile(input);
	string line;
	while (getline(inputFile, line)) {
		int number = stoi(line);

		int result = nextSmallerThan(number);

		outputFile << result << endl;
	}

	inputFile.close();
	outputFile.close();
}

bool nextSmallerThanTests(string input, string output)
{
	ifstream in1(input);
	ifstream in2(output);
	string source1 = "";
	string source2 = "";
	while (getline(in1, source1) && getline(in2, source2))
	{
		int n1 = stoi(source1);
		int n2 = stoi(source2);
		if (n1 != n2)
		{
			in1.close();
			in2.close();
			return false;
		}
	}
	in1.close();
	in2.close();
	return true;
}

void nextSmallerThanTests()
{
	cout << "Test " << (nextSmallerThan(21) == 12 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(531) == 513 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(2071) == 2017 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(9) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(111) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(135) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(1027) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(1113211111) == 1113121111 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(91234567) == 79654321 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(173582) == 173528 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(4321234) == 4314322 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(2147483647) == 2147483476 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThanTests("result.txt", "output.txt") ? "Passed." : "Failed.") << endl;
}